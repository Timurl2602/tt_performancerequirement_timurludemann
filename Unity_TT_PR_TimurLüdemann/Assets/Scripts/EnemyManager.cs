﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTPP
{
    
public class EnemyManager : MonoBehaviour
{
    public string name = "Dragon";
    public float health = 105f;
    public int mana = 50;

    public void Start()
    {
        Debug.Log("The Enemy is " +name+ ".");
        Debug.Log("The health of the " +name+ " is " +health+ ".");
        Debug.Log("The mana of the " +name+ " is " +mana+ ".");
    }
}

}